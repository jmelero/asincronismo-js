// npm install xmlhttprequest --save
// ES6+
let XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

// Función que hace peticiones a la API
const fetchData =  (url_api) => {
    return new Promise((resolve,reject) => {        
            const xhttp = new XMLHttpRequest();
            // Llamar a url
            xhttp.open('GET', url_api, true /* True es asíncrono */);
            // Escuchamos
            xhttp.onreadystatechange = (() => {
                // Tiene 5 estados https://www.w3schools.com/xml/ajax_xmlhttprequest_response.asp
                if (xhttp.readyState === 4 ) {
                    // Comprobar que es correcta
                    (xhttp.status === 200) 
                    ? resolve(JSON.parse(xhttp.responseText))
                    : reject(new Error('Error', url_api))
                }
            });
            xhttp.send();
    });
}

module.exports = fetchData;