// Usamos XMLHttpRequest porque todavía no usamos promesas (fetch)
// npm install xmlhttprequest --save

let XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

const API = 'https://rickandmortyapi.com/api/character/';

// Función que hace peticiones a la API
function fetchData(url_api,callback) {
    let xhttp = new XMLHttpRequest();
    // Llamar a url
    xhttp.open('GET', url_api, true /* True es asíncrono */);
    // Escuchamos
    xhttp.onreadystatechange = function (event) {
        // Tiene 5 estados https://www.w3schools.com/xml/ajax_xmlhttprequest_response.asp
        if (xhttp.readyState === 4 ) {
            // Comprobar que es correcta
            if (xhttp.status === 200 ) {
                // Primero el error
                callback(null, JSON.parse(xhttp.responseText));
            }
            else { 
                const error = new Error('Error ' + url_api);
                return callback(error, null);
            }
        }
    }
    xhttp.send();
}

// Vamos a hacer 3 llamadas con callbacks

fetchData(API,function (error1, data1) {
    if (error1) return console.error(error1);
    fetchData(API + data1.results[0].id, function (error2, data2) {
        if(error2) return console.error(error2);
        fetchData(data2.origin.url, function (error3, data3){
            if(error3) return console.error(error3);
                console.log(data1.info.count);
                console.log(data2.name);
                console.log(data3.dimension);
        });
    })
})