// Una función que al crearla le pasamos otra función (función de orden superior)
// Es esta caso el callback sería la función que es pasada como parámetro

// Estructura

function sum (num1, num2) {
    return num1 + num2;
}

// Pasamos como parámetro una función
function calc(num1,num2, callback) {
    return callback(num1,num2);
}

console.log(calc(6,2, sum));

function date(callback) {
    console.log(new Date);
    setTimeout( function () {
        let date = new Date;
        callback(date);
    }, 3000)
}

function printDate(dateNow) {
    console.log(dateNow);
}

date(printDate);
